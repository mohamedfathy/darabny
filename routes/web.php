<?php

//Home Routes
	Route::get('/', 'HomeController@index')->name('home');

//Users Routes
	Route::get('/user', 'UserController@index')->name('user');
	Route::post('/user/search', 'UserController@search')->name('userSearch');
	Route::get('/user/status/{id}', 'UserController@activation')->name('userActivation');
	Route::get('/user/{id}', 'UserController@show')->name('userShow');

//Companies Routes
	Route::get('/company', 'CompanyController@index')->name('company');
	Route::post('/company/search', 'CompanyController@search');
	Route::get('/company/status/{id}', 'CompanyController@activation')->name('companyActivation');
	Route::get('/company/{id}', 'CompanyController@show');

//Setting Routes
	Route::get('/setting', 'SettingController@index')->name('setting');
	Route::get('/setting/city', 'SettingController@city')->name('city');
	Route::get('/setting/type', 'SettingController@type')->name('type');
	Route::get('/setting/industry', 'SettingController@industry')->name('industry');
	Route::get('/setting/major', 'SettingController@major')->name('major');
	Route::get('/setting/department', 'SettingController@department')->name('department');
	// city operation
	Route::get('/setting/cityAdd', 'SettingController@cityAdd')->name('cityAdd');
	Route::post('/setting/cityCreate', 'SettingController@cityCreate')->name('cityCreate');
	Route::get('/setting/cityEdit/{id}', 'SettingController@cityEdit')->name('cityEdit');
	Route::put('/setting/cityUpdate/{id}', 'SettingController@cityUpdate')->name('cityUpdate');
	Route::get('/setting/cityDestroy/{id}', 'SettingController@cityDestroy')->name('cityDestroy');
	// type operation
	Route::get('/setting/typeAdd', 'SettingController@typeAdd')->name('typeAdd');
	Route::post('/setting/typeCreate', 'SettingController@typeCreate')->name('typeCreate');
	Route::get('/setting/typeEdit/{id}', 'SettingController@typeEdit')->name('typeEdit');
	Route::put('/setting/typeUpdate/{id}', 'SettingController@typeUpdate')->name('typeUpdate');
	Route::get('/setting/typeDestroy/{id}', 'SettingController@typeDestroy')->name('typeDestroy');
	// industry operation
	Route::get('/setting/industryAdd', 'SettingController@industryAdd')->name('industryAdd');
	Route::post('/setting/industryCreate', 'SettingController@industryCreate')->name('industryCreate');
	Route::get('/setting/industryEdit/{id}', 'SettingController@industryEdit')->name('industryEdit');
	Route::put('/setting/industryUpdate/{id}', 'SettingController@industryUpdate')->name('industryUpdate');
	Route::get('/setting/industryDestroy/{id}', 'SettingController@industryDestroy')->name('industryDestroy');
	// major operation
	Route::get('/setting/majorAdd', 'SettingController@majorAdd')->name('majorAdd');
	Route::post('/setting/majorCreate', 'SettingController@majorCreate')->name('majorCreate');
	Route::get('/setting/majorEdit/{id}', 'SettingController@majorEdit')->name('majorEdit');
	Route::put('/setting/majorUpdate/{id}', 'SettingController@majorUpdate')->name('majorUpdate');
	Route::get('/setting/majorDestroy/{id}', 'SettingController@majorDestroy')->name('majorDestroy');
	// department operation
	Route::get('/setting/departmentAdd', 'SettingController@departmentAdd')->name('departmentAdd');
	Route::post('/setting/departmentCreate', 'SettingController@departmentCreate')->name('departmentCreate');
	Route::get('/setting/departmentEdit/{id}', 'SettingController@departmentEdit')->name('departmentEdit');
	Route::put('/setting/departmentUpdate/{id}', 'SettingController@departmentUpdate')->name('departmentUpdate');
	Route::get('/setting/departmentDestroy/{id}', 'SettingController@departmentDestroy')->name('departmentDestroy');

//Contact Routes
	Route::get('/complain', 'ComplainController@index')->name('complain');
	Route::get('/complain/destroy/{id}', 'ComplainController@destroy')->name('complainDestroy');

//contact Routes
	Route::get('/contact', 'ContactController@contactIndex')->name('contact');
	Route::get('/contact/Edit/{id}', 'ContactController@contactEdit')->name('contactEdit');
	Route::put('/contact/Update/{id}', 'ContactController@contactUpdate')->name('contactUpdate');

//about Routes
	Route::get('/about', 'ContactController@aboutIndex')->name('about');
	Route::get('/about/Edit/{id}', 'ContactController@aboutEdit')->name('aboutEdit');
	Route::put('/about/Update/{id}', 'ContactController@aboutUpdate')->name('aboutUpdate');