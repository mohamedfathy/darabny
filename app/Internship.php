<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\Department;
use App\Company;

class Internship extends Model
{
    //

    public function city()
    {
        return $this->belongsTo('App\City', 'id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'id');
    }

}
