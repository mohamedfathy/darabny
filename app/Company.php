<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\Type;
use App\Industry;
use App\Size;

class Company extends Model
{
    //
    public $timestamps = false;

    //relationships
    public function city()
    {
        return $this->belongsTo('App\City', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\Type', 'id');
    }

    public function industry()
    {
        return $this->belongsTo('App\Industry', 'id');
    }

    public function size()
    {
        return $this->belongsTo('App\Size', 'id');
    }

}
