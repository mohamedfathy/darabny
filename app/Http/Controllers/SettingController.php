<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\City;
use App\Type;
use App\Industry;
use App\Major;
use App\Department;
use Flashy;
use Validator;
use Exception;

class SettingController extends Controller
{

    private $rules = [

    ];
    private $messages = [

    ];

    //
    public function index () {
    	$Setting = User::all();
    	return view('setting.index', compact('Setting'));
    }


    public function city () {
    	$Setting = City::all();
    	return view('setting.index', compact('Setting'));
    }


    public function type () {
    	$Setting = Type::all();
    	return view('setting.index', compact('Setting'));
    }


    public function industry () {
    	$Setting = Industry::all();
    	return view('setting.index', compact('Setting'));
    }



    public function major () {
    	$Setting = Major::all();
    	return view('setting.index', compact('Setting'));
    }

    public function department () {
    	$Setting = Department::all();
    	return view('setting.index', compact('Setting'));
    }


    // City Operation
    public function cityAdd() {
        return view('setting.add');
    }
    public function cityCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $city = new City;
        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $handel = $city->save();

        $msgSuccess = "تمت اضافة المدينة بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة المدينة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Setting = City::all();
        return view('setting.index', compact('Setting'));
    }
    public function cityEdit ($id) {
        $Setting = City::find($id);
        return view('setting.edit', compact('Setting'));
    }
    public function cityUpdate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $city = City::find($request->id);
        $city->name_ar = $request->name_ar;
        $city->name_en = $request->name_en;
        $handel = $city->save();
        $msgSuccess = "تم تعديل المدينة بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل المدينة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Setting = City::all();
        return view('setting.index', compact('Setting'));
    } 
    public function cityDestroy ($id) {
        $Setting = false;
        try {
             $Setting = City::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('city');
        }
       
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Setting == true ? $msgSuccess : $msgFailure);
        return redirect()->route('city');
    }

    // Type Operation
    public function typeAdd() {
        return view('setting.add');
    }
    public function typeCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $type = new Type;
        $type->name_ar = $request->name_ar;
        $type->name_en = $request->name_en;
        $handel = $type->save();

        $Setting = Type::all();
        $msgSuccess = "تم اضافة النوع بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة النوع";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        return view('setting.index', compact('Setting'));
    }
    public function typeEdit ($id) {
        $Setting = Type::find($id);
        return view('setting.edit', compact('Setting'));
    }
    public function typeUpdate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $type = Type::find($request->id);
        $type->name_ar = $request->name_ar;
        $type->name_en = $request->name_en;
        $handel = $type->save();
        $msgSuccess = "تم تعديل النوع بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل النوع";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Setting = Type::all();
        return back();
    } 
    public function typeDestroy ($id) {
        $Setting = false;
        try {
             $Setting = Type::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('type');
        }
        
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Setting == true ? $msgSuccess : $msgFailure);
         return redirect()->route('type');
    }


    // Industry Operation
    public function industryAdd() {
        return view('setting.add');
    }
    public function industryCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $industry = new Industry;
        $industry->name_ar = $request->name_ar;
        $industry->name_en = $request->name_en;
        $handel = $industry->save();

        $Setting = Industry::all();
        $msgSuccess = "تمت اضافة الصناعة بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة الصناعة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        return view('setting.index', compact('Setting'));
    }
    public function industryEdit ($id) {
        $Setting = Industry::find($id);
        return view('setting.edit', compact('Setting'));
    }
    public function industryUpdate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $industry = Industry::find($request->id);
        $industry->name_ar = $request->name_ar;
        $industry->name_en = $request->name_en;
        $handel = $industry->save();
        $msgSuccess = "تم تعديل الصناعة بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل الصناعة";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Setting = Industry::all();
        return view('setting.index', compact('Setting'));
    } 
    public function industryDestroy ($id) {
        $Setting = false;
        try {
             $Setting = Industry::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('industry');
        }
        
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Setting == true ? $msgSuccess : $msgFailure);
        return redirect()->route('industry');
    }



    // Major Operation
    public function majorAdd() {
        return view('setting.add');
    }
    public function majorCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required',
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $major = new Major;
        $major->name_ar = $request->name_ar;
        $major->name_en = $request->name_en;
        $handel = $major->save();

        $Setting = Major::all();
        $msgSuccess = "تمت اضافة التخصص بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة التخصص";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        return view('setting.index', compact('Setting'));
    }
    public function majorEdit ($id) {
        $Setting = Major::find($id);
        return view('setting.edit', compact('Setting'));
    }
    public function majorUpdate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $major = Major::find($request->id);
        $major->name_ar = $request->name_ar;
        $major->name_en = $request->name_en;
        $handel = $major->save();
        $msgSuccess = "تم تعديل التخصص بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل التخصص";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Setting = Industry::all();
        return view('setting.index', compact('Setting'));
    } 
    public function majorDestroy ($id) {
        $Setting = false;
        try {
            $Setting = Major::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('major');
        }
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Setting == true ? $msgSuccess : $msgFailure);
        return redirect()->route('major');
    }


    // Department Operation
    public function departmentAdd() {
        return view('setting.add');
    }
    public function departmentCreate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];

        //Validate
        $errors = Validator::make($request->all(), $rules, $messages);
        if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }

        // Create Record
        $department = new Department;
        $department->name_ar = $request->name_ar;
        $department->name_en = $request->name_en;
        $handel = $department->save();

        $Setting = Department::all();
        $msgSuccess = "تمت اضافة القسم بنجاح";
        $msgFailure = "عذرا! لم يتم اضافة القسم";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        return view('setting.index', compact('Setting'));
    }
    public function departmentEdit ($id) {
        $Setting = Department::find($id);
        return view('setting.edit', compact('Setting'));
    }
    public function departmentUpdate(Request $request){
        $rules = [
            'name_ar' =>'required',
            'name_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $department = Department::find($request->id);
        $department->name_ar = $request->name_ar;
        $department->name_en = $request->name_en;
        $handel = $department->save();
        $msgSuccess = "تم تعديل القسم بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل القسم";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Setting = Department::all();
        return view('setting.index', compact('Setting'));
    } 
    public function departmentDestroy ($id) {
        $Setting = false;
        try {
            $Setting = Department::destroy($id);
        } catch (Exception $e) {
            $msgFailure = "لا يمكن الحذف  ... توجد بيانات مرتبطة بهذا الحقل.";
            Flashy::success($msgFailure);
            return redirect()->route('department');
        }

        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Setting == true ? $msgSuccess : $msgFailure);
        return redirect()->route('department');
    }

}
