<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Internship;
use App\Application;
use Flashy;

class CompanyController extends Controller
{

    public function index () {
    	$Company = Company::all();
    	return view('company.index', compact('Company'));
    }

    public function show ($id) {
    	$Company = Company::find($id);
        $Internship = [];
            $Internship = Internship::where('company_id', '=', $id)->get();
            $Num =  Application::where('internship_id', '=', 1)->get()->count();
    	return view('company.show', compact('Company', 'Internship', 'Num'));
    }

    public function search (Request $request) {
        if($request->has('q')){
        $Company =  Company::where('name', 'LIKE', '%'.$request->q.'%')
        ->orWhere('email', 'LIKE', '%'.$request->q.'%')
        ->orWhere('phone', 'LIKE', '%'.$request->q.'%')
        ->get();
        }
      return view('company.index', compact('Company'));
	}
 
	public function activation($id){
	    $Company = Company::find($id);
	    if($Company->is_active == 1){
	      $Company->is_active = 0;
	    }
	    else{$Company->is_active = 1;}
	    $Company->save();
	    $msgSuccess = " تم تفعيل نشاط الشركة بنجاح ";
	    $msgFailure = " تم تعطيل نشاط الشركة بنجاح ";
	    Flashy::success($Company->is_active == 1 ? $msgSuccess : $msgFailure);
	    return back();
    }
}
