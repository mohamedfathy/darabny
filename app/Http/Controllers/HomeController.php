<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Company;
use Charts;

class HomeController extends Controller
{
    

	public function index () {
        // get the count of users/companies 
		$users =  User::get()->count();
		$companies =  Company::get()->count();

        // get the ten recent users/companies 
 		$recent_users = User::orderBy('created_at', 'desc')->take(10)->get();
        $recent_companies = Company::orderBy('created_at', 'desc')->take(10)->get();

        // view the users/companies in charts
		$chart_users = Charts::multiDatabase('areaspline', 'highcharts')
			->title('المستخدمين')
			->colors(['green', '#ffffff'])
            ->dataset('المستخدمين', User::all())
            ->elementLabel("عدد المستخدمين")
            ->dimensions(0, 0)
            ->responsive(true)
            ->lastByDay();

        $chart_companies = Charts::multiDatabase('areaspline', 'highcharts')
			->title('الشركات')
			->colors(['red', '#ffffff'])
            ->dataset('الشركات', Company::all())
            ->elementLabel("عدد الشركات")
            ->dimensions(0, 0)
            ->responsive(true)
            ->lastByDay();

        //return the data to the view  
		return view('home.index', compact('users', 'companies', 'recent_users', 'recent_companies', 'chart_users', 'chart_companies'));
	}

}
