<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Internship;
use App\Application;
use Flashy;

class UserController extends Controller
{

    public function index () {
    	$User = User::all();
    	return view('user.index', compact('User'));
    }

    public function show ($id) {
    	$User = User::find($id);
        $applications = Application::where('user_id', '=', $id)->get();
        $Output = [];
        $Internship = [];
        foreach ($applications as $application) {
            
            $Output[] =  $application;
            $Internship[] = Internship::find($application->internship_id);
        }

    	return view('user.show', compact('User', 'Output', 'Internship'));
    }

    public function search (Request $request) {
        if($request->has('q')){
        $User =  User::where('name', 'LIKE', '%'.$request->q.'%')
        ->orWhere('phone', 'LIKE', '%'.$request->q.'%')
        ->orWhere('address', 'LIKE', '%'.$request->q.'%')
        ->get();
        }
        return view('user.index', compact('User'));
    }
 
    public function activation($id){
        $user = User::find($id);
        if($user->is_active == 1){
        $user->is_active = 0;
        }
        else{$user->is_active = 1;}
        $user->save();
        $ac_msg = " تم تفعيل الحساب بنجاح ";
        $dc_msg = " تم تعطيل الحساب بنجاح ";
        Flashy::success($user->is_active == 1 ? $ac_msg : $dc_msg);
        return back();
    }
}
