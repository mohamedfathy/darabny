<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complain;
use App\User;
use Flashy;

class ComplainController extends Controller
{
    //
    public function index () {
    	$Complain = Complain::all();
    	return view('complain.index', compact('Complain'));
    }

    public function destroy ($id) {
    	$Complain = Complain::destroy($id);
        $msgSuccess = " تمت عملية الحذف بنجاح ";
        $msgFailure = " لا يمكن حذف هذا الحقل ";
        Flashy::success($Complain == true ? $msgSuccess : $msgFailure);
        return back();
    }
}
