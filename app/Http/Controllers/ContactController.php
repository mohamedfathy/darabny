<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Flashy;
use Validator;

class ContactController extends Controller
{
    
    private $rules = [

    ];
    private $messages = [

    ];

    public function contactIndex () {
        $Contact = Contact::find(1);
        return view('contact.index', compact('Contact'));
    }

    public function contactEdit ($id) {
        $Contact = Contact::find($id);
        return view('contact.edit', compact('Contact'));
    }
    public function contactUpdate(Request $request){
        $rules = [
            'contact_email' =>'required',
            'contact_phone' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Contact = Contact::find($request->id);
        $Contact->contact_email = $request->contact_email;
        $Contact->contact_phone = $request->contact_phone;
        $handel = $Contact->save();
        $msgSuccess = "تم تعديل معلومات الاتصال بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل معلومات الاتصال";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Contact = Contact::find(1);
        return view('contact.index', compact('Contact'));
    } 

     public function aboutIndex () {
        $Contact = Contact::find(1);
        return view('about.index', compact('Contact'));
    }

    public function aboutEdit ($id) {
        $Contact = Contact::find($id);
        return view('about.edit', compact('Contact'));
    }
    public function aboutUpdate(Request $request){
        $rules = [
            'about_us_ar' =>'required',
            'about_us_en' =>'required'
        ];
        $messages = [
            'required'  =>'لا بد من ادخال هذا الحقل'
        ];
        //Validate
        $errors = Validator::make($request->all(), $this->rules, $this->messages);
            if($errors->fails()) {
            return redirect()->back()->withErrors($errors)->withInput($request->all());
        }
   
        // Update Record
        $Contact = Contact::find($request->id);
        $Contact->about_us_ar = $request->about_us_ar;
        $Contact->about_us_en = $request->about_us_en;
        $handel = $Contact->save();
        $msgSuccess = "تم تعديل 'عن الموقع' بنجاح";
        $msgFailure = "عذرا! لم يتم تعديل 'عن الموقع'";
        Flashy::success($handel == 1 ? $msgSuccess : $msgFailure);
        $Contact = Contact::find(1);
        return view('about.index', compact('Contact'));
    } 
}
