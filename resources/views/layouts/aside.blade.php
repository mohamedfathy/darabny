<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<div id="sidebar" class="sidebar responsive ace-save-state">
<script type="text/javascript">
try{ace.settings.loadState('sidebar')}catch(e){}
</script>

<ul class="nav nav-list">
	<li class="{{ Request::is(Route::current()->getName() == 'home') ? 'active' : '' }}">
		<a href="{{ route('home') }}">
		<i class="menu-icon fa fa-tachometer"></i>
		<span class="menu-text"> الرئيسية </span>
		</a>
	</li>


	<li class="{{ Request::is('user*') ? 'active' : '' }}">
		<a href="{{ route('user') }}">
		<i class="menu-icon fa fa fa-users"></i>
		<span class="menu-text"> المستخدمين </span>
		</a>
	</li>


	<li class="{{ Request::is('company*') ? 'active' : '' }}">
		<a href="{{ route('company') }}">
		<i class="menu-icon fa fa fa-university"></i>
		<span class="menu-text"> الشركات </span>
		</a>
	</li>




	
	<li class="{{ Request::is('setting*') ? ' active open ' : '' }}">
	<a href="#" class="dropdown-toggle">
	<i class="menu-icon fa fa-bars"></i>
	<span class="menu-text">
	 اعدادات التطبيق 
	</span>
	<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

		<ul class="submenu">
		
		<li class="{{ Request::is(Route::current()->getName() == 'city') ? 'active' : '' }}">
		<a href="{{ route('city') }}">
		<i class="menu-icon fa fa-caret-right"></i>
		<i class="fa fa-id-card-o"></i>
		 المدن
		</a>
		<b class="arrow"></b>
		</li>

		
		<li class="{{ Request::is(Route::current()->getName() == 'type') ? 'active' : '' }}">
		<a href="{{ route('type') }}">
		<i class="menu-icon fa fa-caret-right"></i>
		<i class="fa fa-id-card-o"></i>
		النوع
		</a>
		<b class="arrow"></b>
		</li>

		
		<li class="{{ Request::is(Route::current()->getName() == 'industry') ? 'active' : '' }}">
		<a href="{{ route('industry') }}">
		<i class="menu-icon fa fa-caret-right"></i>
		<i class="fa fa-id-card-o"></i>
		الصناعة
		</a>
		<b class="arrow"></b>
		</li>


		<li class="{{ Request::is(Route::current()->getName() == 'major') ? 'active' : '' }}">
		<a href="{{ route('major') }}">
		<i class="menu-icon fa fa-caret-right"></i>
		<i class="fa fa-id-card-o"></i>
		التخصص
		</a>
		<b class="arrow"></b>
		</li>

		<li class="{{ Request::is(Route::current()->getName() == 'department') ? 'active' : '' }}">
		<a href="{{ route('department') }}">
		<i class="menu-icon fa fa-caret-right"></i>
		<i class="fa fa-id-card-o"></i>
		القسم
		</a>
		<b class="arrow"></b>
		</li>

		</ul>
	</li>



	<li class="{{ Request::is('complain*') ? 'active' : '' }}">
		<a href="{{ route('complain') }}">
		<i class="menu-icon fa fa-commenting-o"></i>
		<span class="menu-text"> الشكاوى و الاقتراحات </span>
		</a>
	</li>


	<li class="{{ Request::is('contact*') ? 'active' : '' }}">
		<a href="{{ route('contact') }}">
		<i class="menu-icon fa fa-phone"></i>
		<span class="menu-text"> اتصل بنا </span>
		</a>
	</li>


	<li class="{{ Request::is('about*') ? 'active' : '' }}">
		<a href="{{ route('about') }}">
		<i class="menu-icon fa fa-exclamation-circle"></i>
		<span class="menu-text"> عن الموقع </span>
		</a>
	</li>
</ul><!-- /.nav-list -->
<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>
</div>