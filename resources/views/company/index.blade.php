@extends ('layouts.master')
@section('title', 'الشركات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<div class="container">
	<form method="POST" action="/company/search" accept-charset="UTF-8" class="form">	

	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
	 {{ csrf_field() }}
      <input placeholder="اسم الشركة / رقم التليفون / الايميل" name="q" type="text" class="form-control search">
    </div><!-- col-xs-10 col-sm-10 col-md-10 col-lg-10 -->

    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    	<input class="btn btn-success form-control" type="submit" value="بحث" >
    </div><!--col-xs-2-->

	</form>
	</div><!-- col-xs-2 col-sm-2 col-md-2 col-lg-2 -->
</div><!--breadcrumbs-->

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>#</th>
			<th>الاسم</th>
			<th>الايميل</th>
			<th>التليفون</th>
			<th>العنوان</th>
			<th>المدينة</th>
			<th>النوع</th>
			<th>المساحة</th>
			<th>الصناعة</th>
			<th>نشاط الشركة</th>
			<th>تاريخ التسجيل</th>
			</tr>
		</thead>
		
		<tbody>
			@if (count($Company) == 0)
				<tr>
					<td colspan="11" style="text-align:center;" ><h3>لا توجد نتائج تطابق بحثك </h3></td>
				</tr>
			@endif
			@foreach ($Company as $C)
			<tr>
			<td>{{$C->id}}</td>
			<td><a href="/company/{{$C->id}}">{{$C->name}} </a></td>
			<td>{{$C->email}}</td>
			<td>{{$C->phone}}</td>
			<td>{{$C->address}}</td>
			<td>{{$C->city->name_ar}}</td>
			<td>{{$C->type->name_ar}}</td>
			<td>{{$C->size->size}}</td>
			<td>{{$C->industry->name_ar}}</td>
			@if ($C->is_active == 0)
				<td class="center">
				<a href="/company/status/{{$C->id}}" class="btn btn-danger btn-xs">موقوف</a>
				</td>
			@endif
			@if ($C->is_active == 1)
				<td class="center">
				<a href="/company/status/{{$C->id}}" class="btn btn-success btn-xs">جارى</a>
				</td>
			@endif
			<td>{{$C->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection