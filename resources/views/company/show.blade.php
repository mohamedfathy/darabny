@extends ('layouts.master')
@section('title', 'الشركات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="container">
<div class="row">
<div class="col-xs-12">

	<br>
	<div class="mydiv">
	<div id="user-profile-1" class="user-profile row">

		<!-- PAGE CONTENT BEGINS -->
		<div class="tabbable">

		<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
			<li class="active">
				<a data-toggle="tab" href="#faq-tab-1" aria-expanded="true">
					<i class="blue ace-icon fa fa fa-user bigger-120"></i>
					معلومات الشركة
				</a>
			</li>

			<li class="">
				<a data-toggle="tab" href="#faq-tab-2" aria-expanded="false">
					<i class="green ace-icon fa fa-credit-card bigger-120"></i>
					المنح المقدمة
				</a>
			</li>
		</ul>

		<div class="tab-content no-border padding-24">
		<div id="faq-tab-1" class="tab-pane fade active in">
			<div class="space-8"></div>

		<!-- PAGE Data BEGINS -->
		<div class="col-xs-12 col-sm-3 center">
			<div class="any">
				<span class="profile-picture">
			<img id="avatar" width="190px" height="230px" class="editable img-responsive" alt="{{$Company->name}}" src="{{ asset((($Company->logo_url == NULL) ? 'profile_company/google.png' : 'profile_company/'.$Company->logo_url) ) }}" />
				</span>
				<div class="space-4"></div><!-- /. space-4 -->

				<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
					<div class="inline position-relative">
					&nbsp;
					<span class="white">{{$Company->name}}</span>
					</div><!-- /. inline position-relative -->
				</div><!-- /. width-80 -->
			</div><!-- /. any -->
			<div class="space-6"></div><!-- /. space-6 -->
		</div><!-- /. col-xs-12 col-sm-3 center -->


		<div class="col-xs-12 col-sm-9">
				<div class="space-12"></div><!-- /. space-12 -->
				<div class="profile-user-info profile-user-info-striped">

				<div class="profile-info-row">
					<div class="profile-info-name"> الاسم </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="country">{{$Company->name}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الايميل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="age">{{$Company->email}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> رقم التليفون </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="signup">{{$Company->phone}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> المدينة  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<i class="fa fa-map-marker light-orange bigger-110"></i>
					<span class="editable" id="login">{{$Company->address}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> النوع  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="login">{{$Company->type->name_ar}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->


				<div class="profile-info-row">
					<div class="profile-info-name"> الحجم  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="login">{{$Company->size->size}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الصناعة  </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="login">{{$Company->industry->name_ar}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> الحالة </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
						<span class="editable" id="about">
						@if ($Company->is_active == 0)
						<span class="label label-danger">غير مفعل</span>
						@endif
						@if ($Company->is_active == 1)
						<span class="label label-success">مفعل</span>@endif</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				<div class="profile-info-row">
					<div class="profile-info-name"> تاريخ التسجيل </div><!-- /. profile-info-name -->
					<div class="profile-info-value">
					<span class="editable" id="about">{{$Company->created_at}}</span>
					</div><!-- /. profile-info-value -->
				</div><!-- /. profile-info-row -->

				</div><!-- /. profile-user-info profile-user-info-striped -->
		</div><!-- /. col-xs-12 col-sm-9 -->

		<!-- PAGE Data ENDS -->

		</div><!--faq-tab-1-->

		<div id="faq-tab-2" class="tab-pane fade">
			<div class="space-8"></div>

				<div class="row">
				<div class="col-xs-12">
					<table id="simple-table" class="table table-bordered table-hover">
						<thead>
							<tr>
							<th>#</th>
							<th>عنوان المنحة</th>
							<th>المدينة</th>
							<th>القسم</th>
							<th>من</th>
							<th>الى</th>
							<th>اسم الشركة</th>
							<th>حالة المنحة</th>
							<th>عدد المتقدمين</th>
							<th>تاريخ الاضافة</th>
							</tr>
						</thead>
						<tbody>
							@for ($i = 0; $i < count($Internship); $i++)
							<tr>
							<td>{{$Internship[$i]->id}}</td>
							<td>{{$Internship[$i]->title}}</td>
							<td>{{$Internship[$i]->city->name_ar}}</td>
							<td>{{$Internship[$i]->department->name_ar}}</td>
							<td>{{$Internship[$i]->from}}</td>
							<td>{{$Internship[$i]->to}}</td>
							<td>{{$Internship[$i]->company->name}}</td>
							@if ($Internship[$i]->is_active == 1)
								<td><span class="badge badge-success">متاحة</span></td>
							@else
								<td><span class="badge badge-danger">غير متاحة</span></td>
							@endif
							<td style="text-align:center;"><span class="badge badge-primary">{{$Num}}</span></td>
							<td>{{$Internship[$i]->created_at}}</td> 
							</tr>
							@endfor
						</tbody>
					</table>
				</div><!-- /.col-xs-12 -->
				</div><!-- /.row -->
        
		</div>

		</div>
		</div>
		<!-- PAGE CONTENT ENDS -->


	</div><!-- /# user-profile-1 -->
	</div><!-- /. mydiv -->

</div><!-- /.container -->
</div><!-- /.row -->
</div><!-- /.col-xs-12 -->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection