@extends ('layouts.master')
@section('title', 'الشكاوى و الاقتراحات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">


<br>
<div class="container">
<div class="row">
<div class="col-xs-12">
	<h1># الشكاوى و الاقتراحات</h1>
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>#</th>
			<th>الاسم</th>
			<th class="center">الرسالة</th>
			<th>تاريخ الارسال</th>
			<th>العملية</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach ($Complain as $C) 
			<tr>
			<td>{{$C->id}}</td>
			<td><a href="/users/{{$C->user->id}}">{{$C->user->name}}</a></td>
			<td>{{$C->message}}</td>
			<td>{{$C->timestamp}}</td>
			<td><a href="/complain/destroy/{{$C->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection