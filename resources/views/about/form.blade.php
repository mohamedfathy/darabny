<div class="form-group has-float-label col-sm-6">
	<label for="name">	عن الموقع عربى</label>
    {{ Form::text('about_us_ar', old('about_us_ar'), ['placeholder' => 'عن الموقع عربى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('about_us_ar') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('about_us_ar') ? $errors->first('about_us_ar') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
	<label for="email">عن الموقع انجليزى	</label>
    {{ Form::text('about_us_en', old('about_us_en'), ['placeholder' => 'عن الموقع انجليزى', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('name_en') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('about_us_en') ? $errors->first('about_us_en') : '' }}</small>
</div>



<div class="form-group col-sm-12 submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>