@extends ('layouts.master')
@section('title', 'تعديل')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">


	<div class="page-content">
     <div class="col-md-12">
		<div class="page-header">
      		<h1><i class="menu-icon fa fa-magic"></i> اضافة معلومات الموقع</h1>
      	</div>
	 </div>
	 <div class="row">
		<div class="col-xs-12">
			{{ Form::model($Contact, ['route' => ['aboutUpdate' , $Contact->id], 'class' => 'form', 'method' => 'PUT']) }}
			@include('about.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'Contact' => $Contact])
			{{ Form::close() }}
		</div>
	 </div>
    </div>



</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection