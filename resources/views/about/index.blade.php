@extends ('layouts.master')
@section('title', 'عن الموقع')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<br>
<div class="col-xs-12">
<div class="pull-right">
	<h1># عن الموقع</h1>
</div>
<div class="pull-left">
</div>
</div>

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>#</th>
			<th>عن الموقع عربى</th>
			<th>عن الموقع انجليزى</th>
			<th>تعديل</th>
			</tr>
		</thead>
		
		<tbody>
			<tr>
			<td>{{$Contact->id}}</td>
			<td>{{$Contact->about_us_ar}}</td>
			<td>{{$Contact->about_us_en}}</td>
			<td><a href="/about/Edit/{{$Contact->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			</tr>
		</tbody>
	</table>
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection