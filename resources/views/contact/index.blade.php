@extends ('layouts.master')
@section('title', 'اتصل بنا')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">
	<h1># اتصل بنا</h1>
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>#</th>
			<th>رقم تليفون الموقع</th>
			<th>ايميل الموقع</th>
			<th>تعديل</th>
			</tr>
		</thead>
		
		<tbody>
			<tr>
			<td>{{$Contact->id}}</td>
			<td>{{$Contact->contact_phone}}</td>
			<td>{{$Contact->contact_email}}</td>
			<td><a href="/contact/Edit/{{$Contact->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			</tr>
		</tbody>
	</table>
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection