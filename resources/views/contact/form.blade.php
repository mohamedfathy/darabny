<div class="form-group has-float-label col-sm-6">
	<label for="name">	رقم تليفون الموقع</label>
    {{ Form::text('contact_phone', old('contact_phone'), ['placeholder' => 'رقم تليفون الموقع', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('	contact_phone') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('	contact_phone') ? $errors->first('	contact_phone') : '' }}</small>
</div>


<div class="form-group has-float-label col-sm-6">
	<label for="email">ايميل الموقع</label>
    {{ Form::email('contact_email', old('contact_email'), ['placeholder' => 'ايميل الموقع', 'required' => 'required', 'class' => 'form-control ' . ($errors->has('contact_email') ? 'redborder' : '') ]) }}
    <small class="text-danger">{{ $errors->has('contact_email') ? $errors->first('contact_email') : '' }}</small>
</div>




<div class="form-group col-sm-12 submit pull-left">
    <p>{{ Form::submit($btn , ['class' => 'btn btn-lg col-sm-3 pull-left btn-primary' . $classes ]) }}
</div>