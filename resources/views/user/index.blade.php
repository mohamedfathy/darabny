@extends ('layouts.master')

@section('title', 'المستخدمين')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<div class="container">
		<form method="POST" action="/user/search" accept-charset="UTF-8" class="form">	

			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
			 {{ csrf_field() }}
		      <input placeholder="اسم المستخدم / رقم التليفون / المدينة" name="q" type="text" class="form-control search">
		    </div><!-- col-xs-10 col-sm-10 col-md-10 col-lg-10 -->

		    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		    	<input class="btn btn-success form-control" type="submit" value="بحث" >
		    </div><!--col-xs-2-->

		</form>
	</div><!-- col-xs-2 col-sm-2 col-md-2 col-lg-2 -->
</div><!--breadcrumbs-->

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>#</th>
			<th>الاسم</th>
			<th>الايميل</th>
			<th>التليفون</th>
			<th>المدينة</th>
			<th>القرية</th>
			<th>الحاله</th>
			<th>تاريخ التسجيل</th>
			</tr>
		</thead>
		
		<tbody>
			@if (count($User) == 0)
				<tr>
					<td colspan="8" style="text-align:center;" ><h3>لا توجد نتائج تطابق بحثك </h3></td>
				</tr>
			@endif

			@foreach ($User as $U)
			<tr> 
			<td>{{$U->id}}</a></td>
			<td><a href="/user/{{$U->id}}">{{$U->name}} </a></td>
			<td>{{$U->email}}</td>
			<td>{{$U->phone}}</td>
			<td>{{$U->address}}</td>
			<td>{{$U->city->name_ar}}</td>
			@if ($U->is_active == 0)
			<td class="center">
			<a href="/user/status/{{$U->id}}" class="btn btn-danger btn-xs">غير مفعل</a>
			</td>
			@endif
			@if ($U->is_active == 1)
			<td class="center">
			<a href="/user/status/{{$U->id}}" class="btn btn-success btn-xs">مفعل</a>
			</td>
			@endif
			<td>{{$U->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection