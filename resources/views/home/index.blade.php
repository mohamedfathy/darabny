@extends ('layouts.master')
@section('title', 'الرئيسية')
@section ('content')
<div class="main-content">
<div class="main-content-inner">
<div class="row">
<div class="col-xs-12">
  <div class="statistics">
    <div class="statistics_box">
      <h1>{{$users}}  <i class="fa fa-user" aria-hidden="true"></i></h1>
      <h1>مستخدم</h1>
    </div><!--statistics_box-->
    <div class="statistics_box">
      <h1>{{$companies}} <i class="fa fa-university" aria-hidden="true"></i></h1>
      <h1>شركة</h1>
    </div><!--statistics_box-->
  </div><!--statistics-->

  {!! Charts::styles() !!}

  <div class="container">
    <div class="col-md-11">
      <div class="page-header">
         <h3 class="charts_header">اجمالى عدد المستخدمين : ({{$users}})</h3>
      </div>
      <div class="charts_border">
           {!! $chart_users->html() !!}
      </div><!--charts_border-->                 
    </div><!--col-md-11-->
  </div><!--container-->


  <div class="container">
    <div class="col-md-11">
      <div class="page-header">
         <h3 class="charts_header">اجمالى عدد الشركات : ({{$companies}})</h3>
      </div>
      <div class="charts_border">
           {!! $chart_companies->html() !!}
      </div><!--charts_border-->             
    </div><!--col-md-11-->
  </div><!--container-->

<br />

  <div class="container">
  <div class="col-md-11">
  <div class="row">

    <div class="col-xs-12 col-sm-6 widget-container-col" 
    id="widget-container-col-2">
    <div class="widget-box widget-color-blue" id="widget-box-2">

      <div class="widget-header">
        <h5 class="widget-title bigger lighter">
        <i class="fa fa-users" aria-hidden="true"></i>
        احدث المستخدمين
        </h5>
      </div><!--/.widget-header-->

      <div class="widget-body">
      <div class="widget-main no-padding">
        <table class="table table-striped table-bordered table-hover">
        <thead class="thin-border-bottom">
        <tr>
          <th><i class="ace-icon fa fa-user"></i> مستخدم </th>
          <th><i class="fa fa-calendar"></i> تاريخ التسجيل </th>
          <th class="hidden-480"><i class="fa fa-eye"></i> حالة المستخدم </th>
        </tr>
        </thead>

        <tbody>
        @foreach ($recent_users as $recent_user)
        <tr>
          <td><a href="/user/{{$recent_user->id}}">{{$recent_user->name}}</a></td> 
          <td>{{$recent_user->created_at}}</td>

          @if ($recent_user->is_active == 1) 
            <td><span class="label label-success">مفعل</span></td>
          @endif
          @if ($recent_user->is_active == 0) 
          <td><span class="label label-danger">غير مفعل</span></td>
          @endif
          </tr>
        @endforeach
        </tbody>
      </table><!--table table-striped-->

      </div><!-- /.widget-main -->
      </div><!-- /.widget-body -->
      
      </div><!-- /.widget-box -->
      </div><!-- /.col-xs-12 col-sm-6 widget-container-col-->


    <div class="col-xs-12 col-sm-6 widget-container-col" id="widget-container-col-2">
    <div class="widget-box widget-color-blue" id="widget-box-2">

      <div class="widget-header">
        <h5 class="widget-title bigger lighter">
        <i class="fa fa-university" aria-hidden="true"></i> احدث الشركات </h5>
      </div><!-- /.widget-header-->

      <div class="widget-body">
      <div class="widget-main no-padding">
      <table class="table table-striped table-bordered table-hover">

        <thead class="thin-border-bottom">
        <tr>
          <th><i class="ace-icon fa-university"></i> شركة </th>
          <th><i class="fa fa-calendar"></i> تاريخ التسجيل </th>
          <th class="hidden-480"><i class="fa fa-eye"></i> نشاط الشركة </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($recent_companies as $recent_company)
        <tr>
        <td><a href="/companys/{{$recent_company->id}}">{{$recent_company->name}} </a></td> 
        <td>{{$recent_company->created_at}}</td>

        @if ($recent_company->is_active == 1) 
        <td><span class="label label-success">جارى</span></td>
        @endif
        @if ($recent_company->is_active == 0) 
        <td><span class="label label-danger">موقوف</span></td>
        @endif
        </tr>
        @endforeach
      </tbody>
  </table>

  </div><!-- /.widget-main -->
  </div><!-- /.widget-body -->

  </div><!-- /.widget-box -->
  </div><!-- /.col-xs-12 col-sm-6 widget-container-col -->


  </div><!-- /.row -->
  </div><!-- /col-md-11 -->
  </div><!-- /.container -->

    {!! Charts::scripts() !!}
    {!! $chart_users->script() !!}
    {!! $chart_companies->script() !!}

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection