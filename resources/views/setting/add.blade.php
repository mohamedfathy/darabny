@extends ('layouts.master')
@section('title', 'اضافة')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">

	<div class="page-content">
     <div class="col-md-12">
		<div class="page-header">
		@if(Route::current()->getName() == 'setting')
		<h1># الاعدادات</h1>
		@elseif (Route::current()->getName() == 'cityAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة مدينة</h1>
		@elseif (Route::current()->getName() == 'typeAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة نوع</h1>
		@elseif (Route::current()->getName() == 'industryAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة صناعة</h1>
		@elseif (Route::current()->getName() == 'majorAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة تخصص</h1>
		@elseif (Route::current()->getName() == 'departmentAdd')
		<h1><i class="menu-icon fa fa-magic"></i> اضافة قسم</h1>
		@endif
      	</div>
	 </div>
	 <div class="row">
		<div class="col-xs-12">

			@if(Route::current()->getName() == 'setting')
			<td># الاعدادات</td>
			@elseif (Route::current()->getName() == 'cityAdd')
				{{ Form::open(['route' => 'cityCreate', 'class' => 'form']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
				{{ Form::close() }}
			@elseif (Route::current()->getName() == 'typeAdd')
				{{ Form::open(['route' => 'typeCreate', 'class' => 'form']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
				{{ Form::close() }}
			@elseif (Route::current()->getName() == 'industryAdd')
				{{ Form::open(['route' => 'industryCreate', 'class' => 'form']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
				{{ Form::close() }}
			@elseif (Route::current()->getName() == 'majorAdd')
				{{ Form::open(['route' => 'majorCreate', 'class' => 'form']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
				{{ Form::close() }}
			@elseif (Route::current()->getName() == 'departmentAdd')
				{{ Form::open(['route' => 'departmentCreate', 'class' => 'form']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary'])
				{{ Form::close() }}
			@endif

		</div>
	 </div>
    </div>

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection