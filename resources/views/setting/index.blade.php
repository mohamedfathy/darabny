@extends ('layouts.master')
@section('title', 'الاعدادات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="pull-right">
	@if(Route::current()->getName() == 'setting')
	<h1># الاعدادات</h1>
	@elseif (Route::current()->getName() == 'city' OR Route::current()->getName() == 'cityCreate' OR Route::current()->getName() == 'cityUpdate')
	<h1># المدينة</h1>
	@elseif (Route::current()->getName() == 'type' OR Route::current()->getName() == 'typeCreate' OR Route::current()->getName() == 'typeUpdate')
	<h1># النوع</h1>
	@elseif (Route::current()->getName() == 'industry' OR Route::current()->getName() == 'industryCreate' OR Route::current()->getName() == 'industryUpdate')
	<h1># الصناعة</h1>
	@elseif (Route::current()->getName() == 'major' OR Route::current()->getName() == 'majorCreate' OR Route::current()->getName() == 'majorUpdate')
	<h1># التخصص</h1>
	@elseif (Route::current()->getName() == 'department' OR Route::current()->getName() == 'departmentCreate' OR Route::current()->getName() == 'departmentUpdate')
	<h1># القسم</h1>
@endif
</div>
<div class="pull-left">
@if(Route::current()->getName() == 'setting')
	<h1># الاعدادات</h1>
	@elseif (Route::current()->getName() == 'city') 
	<a href="/setting/cityAdd" class="btn btn-primary">اضافة مدينة</a>
	@elseif (Route::current()->getName() == 'type')
	<a href="/setting/typeAdd" class="btn btn-primary">اضافة نوع</a>
	@elseif (Route::current()->getName() == 'industry')
	<a href="/setting/industryAdd" class="btn btn-primary">اضافة صناعة</a>
	@elseif (Route::current()->getName() == 'major')
	<a href="/setting/majorAdd" class="btn btn-primary">اضافة تخصص</a>
	@elseif (Route::current()->getName() == 'department')
	<a href="/setting/departmentAdd" class="btn btn-primary">اضافة قسم</a>
@endif
</div>
</div>

<div class="col-xs-12">
	<table id="simple-table" class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>#</th>
			<th>عربى</th>
			<th>انجليزى</th>
			<th>تعديل</th>
			<th>حذف</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach ($Setting as $S)
			<tr>
			<td>{{$S->id}}</td>
			<td>{{$S->name_ar}}</td>
			<td>{{$S->name_en}}</td>
			@if(Route::current()->getName() == 'setting')
			<td># الاعدادات</td>
			@elseif (Route::current()->getName() === 'city' OR Route::current()->getName() == 'cityCreate' OR Route::current()->getName() == 'cityUpdate')
			<td><a href="/setting/cityEdit/{{$S->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/setting/cityDestroy/{{$S->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			@elseif (Route::current()->getName() == 'type' OR Route::current()->getName() == 'typeCreate' OR Route::current()->getName() == 'typeUpdate')
			<td><a href="/setting/typeEdit/{{$S->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/setting/typeDestroy/{{$S->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			@elseif (Route::current()->getName() == 'industry' OR Route::current()->getName() == 'industryCreate' OR Route::current()->getName() == 'industryUpdate')
			<td><a href="/setting/industryEdit/{{$S->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/setting/industryDestroy/{{$S->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			@elseif (Route::current()->getName() == 'major' OR Route::current()->getName() == 'majorCreate' OR Route::current()->getName() == 'majorUpdate')
			<td><a href="/setting/majorEdit/{{$S->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/setting/majorDestroy/{{$S->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			@elseif (Route::current()->getName() == 'department' OR Route::current()->getName() == 'departmentCreate' OR Route::current()->getName() == 'departmentUpdate')
			<td><a href="/setting/departmentEdit/{{$S->id}}" class="btn btn-success btn-xs">تعديل</a></td>
			<td><a href="/setting/departmentDestroy/{{$S->id}}" class="btn btn-danger btn-xs">حذف</a></td>
			@endif
			@endforeach
		</tbody>
	</table>
</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection