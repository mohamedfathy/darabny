@extends ('layouts.master')
@section('title', 'تعديل الاعدادات')
@section ('content')
<div class="main-content">
<div class="main-content-inner">

<br>
<div class="container">
<div class="row">
<div class="col-xs-12">

	<div class="page-content">
     <div class="col-md-12">
		<div class="page-header">
      	@if(Route::current()->getName() == 'setting')
		<h1># الاعدادات</h1>
		@elseif (Route::current()->getName() == 'cityEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل مدينة</h1>
		@elseif (Route::current()->getName() == 'typeEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل نوع</h1>
		@elseif (Route::current()->getName() == 'industryEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل صناعة</h1>
		@elseif (Route::current()->getName() == 'majorEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل تخصص</h1>
		@elseif (Route::current()->getName() == 'departmentEdit')
		<h1><i class="menu-icon fa fa-magic"></i> تعديل قسم</h1>
		@endif
      	</div>
	 </div>
	 <div class="row">
		<div class="col-xs-12">
			@if(Route::current()->getName() == 'setting')
			<td># الاعدادات</td>
			@elseif (Route::current()->getName() == 'cityEdit')
				{{ Form::model($setting, ['route' => ['cityUpdate' , $setting->id], 'class' => 'form', 'method' => 'PUT']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'setting' => $setting])
				{{ Form::close() }}


			@elseif (Route::current()->getName() == 'typeEdit')
				{{ Form::model($setting, ['route' => ['typeUpdate' , $setting->id], 'class' => 'form', 'method' => 'PUT']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'setting' => $setting])
				{{ Form::close() }}


			@elseif (Route::current()->getName() == 'industryEdit')
				{{ Form::model($setting, ['route' => ['industryUpdate' , $setting->id], 'class' => 'form', 'method' => 'PUT']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'setting' => $setting])
				{{ Form::close() }}


			@elseif (Route::current()->getName() == 'majorEdit')
				{{ Form::model($setting, ['route' => ['majorUpdate' , $setting->id], 'class' => 'form', 'method' => 'PUT']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'setting' => $setting])
				{{ Form::close() }}

				
			@elseif (Route::current()->getName() == 'departmentEdit')
				{{ Form::model($setting, ['route' => ['departmentUpdate' , $setting->id], 'class' => 'form', 'method' => 'PUT']) }}
					@include('setting.form', ['btn' => 'حفظ', 'classes' => 'btn btn-primary', 'setting' => $setting])
				{{ Form::close() }}
			@endif
		</div>
	 </div>
    </div>

</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
</div><!--/.container-->

</div><!-- /.main-content-inner -->
</div><!-- /.main-content -->
@endsection